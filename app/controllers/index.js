export default Ember.ArrayController.extend(Ember.Validations.Mixin, {
validations: {
    name: {
      presence: { message: "Field is  very required." },
      length: { minimum: 3, messages: { tooShort: "Must be at least 3 characters." } }
    },
        name2: {
            presence: { message: "Field is  ALSO required." },
      length: { minimum: 7, messages: { tooShort: "Must be at least 7 characters." } }
        },
        name3: {
            numericality: { odd: true, messages: { odd: 'must be an odd number' } }
        },
        name4: {format: { with: /^([a-zA-Z]|\d)+$/, allowBlank: true, message: 'must be letters and numbers only'  } 
        },
        name5:{ format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , allowBlank: true, message: 'must be email'  }
        },
        name6:{ format: { with: /^[0-9]{7,9}$/, allowBlank: true, message: 'must be phone'  }
        }
        
  },
  actions: {
    GetData: function(){
      console.log(this.get('name'));
      console.log(this.get('name1'));
      console.log(this.get('name2'));
      console.log(this.get('name3'));
      console.log(this.get('name4'));
      console.log(this.get('name5'));
      console.log(this.get('name6'));
    }
  }
});